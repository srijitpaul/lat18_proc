% Please make sure you insert your
% data according to the instructions in PoSauthmanual.pdf

\documentclass{PoS}

\title{Towards the P-wave nucleon-pion scattering amplitude in the $\Delta (1232)$ channel: Phase shift analysis}

\ShortTitle{$N-\pi$ scattering: phase shift analysis}
\author{\speaker{Srijit Paul}$\;^{ab}$,Constantia Alexandrou$\;^{ac}$, Giannis Koutsou$\;^{a}$, Stefan Krieg$\;^{bd}$, Luka Leskovec$\;^{e}$, Stefan Meinel$\;^{fg}$, John W. Negele$\;^{h}$, Marcus Petschlies$\;^{i}$, Andrew Pochinsky$\;^{h}$, Gumaro Rendon$\;^{f}$, Giorgio Silvi$\;^{d}$ and Sergey Syritsyn$^{j}$\\
\llap{$^a$}Computation-based Science and Technology Research Center, The Cyprus Institute \\
20 Kavafi Str., Nicosia, 2121, Cyprus\\
\llap{$^b$}Faculty of Mathematics und Natural Sciences, University of Wuppertal\\
Wuppertal-42119, Germany\\
\llap{$^c$}Department of Physics, University of Cyprus, POB 20537, 1678 Nicosia, Cyprus\\
\llap{$^d$}Institute for Advanced Simulation, Forschungszentrum J\"ulich GmbH, J\"ulich 52425, Germany\\
\llap{$^e$}Theory Center, Jefferson Lab, Newport News, VA 23606, USA\\
\llap{$^f$}Department of Physics, University of Arizona, Tucson, AZ 85721, USA\\
\llap{$^g$}RIKEN BNL Research Center, Brookhaven National Laboratory, Upton, NY 11973, USA\\
\llap{$^h$}Center for Theoretical Physics, Massachusetts Institute of Technology\\
Cambridge, MA 02139, USA\\
\llap{$^i$}Helmholtz-Institut für Strahlen- und Kernphysik, University of Bonn, Bonn 53115, Germany\\
\llap{$^j$}Department of Physics and Astronomy, Stony Brook University, Stony Brook, NY 11794, USA\\
E-mail: \email{s.paul@hpc-leap.eu}, \email{alexand@ucy.ac.cy}, \email{koutsou@cyi.ac.cy}, \email{s.krieg@fz-juelich.de}, \email{leskovec@jlab.org}, \email{smeinel@email.arizona.edu}, \email{negele@mit.edu}, \email{marcus.petschlies@hiskp.uni-bonn.de}, \email{avp@mit.edu}, \email{jgrs@email.arizona.edu}, \email{g.silvi@fz-juelich.de}, \email{ssyritsyn@quark.phy.bnl.gov} }


\abstract{We use Lattice QCD and the L\"uscher method to study elastic $\pi$-$N$ scattering in the $I = 3/2$ channel and their coupling to the $\Delta$ resonance. With $m_\pi = 250$ MeV  and lattice size $3.7$ fm , the strong decay channel $\Delta \rightarrow \pi N$ is open. We present our preliminary data for the phase shift across the resonance region and discuss the extraction of the resonance $m_\Delta$ as well as the $g_{\Delta-\pi N}$ coupling.}

\FullConference{The 36th Annual International Symposium on Lattice Field Theory - LATTICE2018\\
		22-28 July, 2018\\
		Michigan State University, East Lansing, Michigan, USA.}
\usepackage{gensymb}
\usepackage{bbm}
\usepackage{amsmath}
\usepackage{longtable}
\usepackage{hhline}
\usepackage{booktabs}
\bibliographystyle{JHEP}

\graphicspath{{images/}}
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
The study of scattering of strongly-interacting hadrons on the lattice, has given us quantitative theoretical insights on unstable hadrons, which are otherwise difficult to analyze perturbatively. Even though the theoretical foundations for studying the $2\rightarrow 2$ scattering resonances on the lattice were laid in 1990s by L\"uscher\cite{L_scher_1990}, the practical implementation of these algorithms along with advancement of solvers(multigrid) and the availability of physical point lattices took two decades. In the last decade, there have been extensive studies of low-lying meson resonances, starting with the $\rho$\cite{ Alexandrou:2017mpi, Wilson:2015dqa} meson
which served as the first evidence for the practical applicability of the L\"uscher methodology to extract resonance parameters. The next evident step is to study the more complex $2\rightarrow 2$ strongly interacting scattering systems, which involves the low-lying baryon resonances. $\Delta(1232)$ is the lowest lying baryon resonance.\\ 
The $\Delta$ baryon is an isospin $3/2$ particle with a resonance mass of $\approx 1210$ MeV and its decay width is $\Gamma_{\Delta\to N\pi} \approx 117$ MeV. The dominant decay mode is in the $I=3/2$ elastic $P$-wave $N\pi$ channel with a branching Fraction of $99.4\%$; PDG only lists one other decay mode - $N\gamma$ with a branching fraction $0.6\%$. \\
In our calculations we make use of moving frames to better determine the infinite volume scattering amplitudes. In the case of the $\Delta$ this brings up a slight complication - because the $N\pi$ channel Lorentz transformations lead to partial wave mixing in the spectrum determined on the lattice. We must thus determine not only the $J=3/2, L=1$ amplitude where the $\Delta$ appears as a resonance, but rather also the $J=3/2, L=2$, $J=1/2,L=0$ and $J=1/2,L=1$ amplitudes. Fortunately these remain non-resonant in the energy region below $\approx 1.6$ GeV. Previous studies of the $\Delta$ coupling to the $N\pi$ channel have used the Michael-McNeille method to determine the coupling \cite{Alexandrou:2013ata} as well as the L\"uscher method \cite{Verduci:2014btc,Andersen:2017una}, however none have yet taken the full partial wave mixing into account. In this preliminary work we neglected the mixing in analogy with the previous calculations, but we plan to extend to account for partial-wave mixing, for the full fledged calculation.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Lattice Setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Lattice Setup}
The parameters of the lattice gauge-field ensemble are given in Table \ref{tab:lattice}.
The gluon action is a tadpole-improved tree-level improved Symanzik action \cite{Symanzik_1983_1,
Symanzik_1983, L_scher_1985}. We use the same clover-improved Wilson
action \cite{Wilson:1974sk, Sheikholeslami_1985} for the sea and valence quarks. The
gauge links in the fermion action are smeared using two levels of HEX smearing as motivated by \cite{Durr:2010aw}. The strange-quark mass is consistent with its physical value as indicated by the '`$\eta_s$'' mass\cite{Davies_2010, Dowdall_2012}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}[htb]
\centering
 \small
 \caption{Lattice parameters for Isotropic lattice($24^3\times 48$); $N_f = 2 + 1$ Clover fermions;}
\begin{tabular}{lllll}\hline 
		  $a$(fm) & L(fm) & $m_\pi$(MeV) &$m_\pi L$ &$N_{config}$ \\ \hline
		 $0.116$  & $2.8$ & $254(1)$ &$3.6$ & $600$ \\ 
		\hline 
\end{tabular}
\label{tab:lattice}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Summary of pi-N spectrum
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Summary of $\pi-N$ spectrum}
The relevant single and multi-hadron interpolators of $\Delta$ and $\pi N$ respectively, in each moving frame, were constructed using the projection method as discussed in \cite{Morningstar:2013bda}.The two point functions between the interpolators enable us to extract the discrete energy spectrum of the pion nucleon interactions on the lattice. A comprehensive list of all the contractions are compiled in Fig.~\ref{fig:contract1}.
%
\begin{figure}[!htb]
\centering
\includegraphics[width=0.43\textwidth]{wick_2pt_2.pdf}%
\hfill
\includegraphics[width=0.37\textwidth]{wick_2pt_test.pdf}
\caption{Left panel: Two point function contractions. Grey circles represent $\Delta$, green circles are $\pi$, blue circles are $N$. The red border on circles represents point source. The dotted border represents sequential source. The black arrowed lines represent one-to-all propagator. The red arrowed lines represent sequential propagator. Right panel: Two point function contractions, the rest is the same as in left panel. The blue arrowed lines represent stochastic propagator.}
\label{fig:contract1}
\end{figure}
\\ In order to reliably extract independent energy levels from the two-point correlation functions of the interpolating fields, GEVP was implemented. 
The $\pi N$ spectrum for different irreps of the center of mass and other moving frames were obtained with a stability analysis of the single exponential fits. The GEVP results are shown in Fig.~\ref{fig:GEVP}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%L\"uscher Methodology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{L\"uscher Methodology}
The two body L{\"u}scher quantization condition \cite{Briceno:2017max}, which connects finite volume energy levels with infinite volume scattering phase shifts of two particles is given by,
%

\begin{equation}
{\rm det} \bigg( \mathbbm{1} + i t_{\ell}(s)(\mathbbm{1} + i {M}^{\vec{P}}) \bigg) = 0,
\end{equation}
%
where  $t_{\ell}(s) = \frac{1}{\cot{\delta_{\ell}(s)}-i}.$ Using the $t$-matrix parametrization and the total angular momentum basis of the interacting particles, we obtain\
\begin{equation}
\label{eq:QC}
\det[M^{\vec{P}}_{Jlm,J^{\prime}l^{\prime}m^{\prime}}-\delta_{JJ^{\prime}}\delta_{ll^{\prime}}\delta_{mm^{\prime}}\cot\delta_{Jl}]=0.
\end{equation}
where $M^{\vec{P}}_{Jlm,J^{\prime}l^{\prime}m^{\prime}}$, contains the finite volume spectra for scattering of two particles with spins $\vec{\rm\bf s_1}$ and $\vec{\rm\bf s_2}$, having total linear momentum $\vec{P}$, the angular momentum $\vec{\rm \bf l}$ which is the contribution from $l$th partial wave such that the total angular momentum $\vec{\rm \bf J} = \vec{\rm \bf l} + \vec{\rm \bf s_1} + \vec{\rm \bf s_2}$, and for a fixed $J$ and $l$, $-J\leq m\leq J$. Thus, $M$ matrix represents the mixing of the different angular momenta in finite volume. $\delta_{Jl}$ represents the infinite volume phase shift for a given $J$ and $l$. It becomes quite evident from addition of angular momenta that, by definition $M$ is a infinite-dimensional matrix because of the infinitely many possible values of $\vec{\rm \bf l}$. In order to make a lattice calculation, we need to select a $l_{max}$, and ignore higher partial waves. This can be justified because at small center-of-mass momenta $p^*$, $\delta(p^*)\propto (p^*)^{2l+1}$.  \\
After neglecting higher partial waves, the finite dimensional $M$ matrix can be further simplified into a block diagonal form through a basis transformation of the Irreps of the symmetry group of the lattice. Given a lattice symmetry group $\Gamma$ with irrep $\alpha$ ( from Table.~\ref{plan}), the $M$ matrix element in the new basis can be written as
\begin{equation}
\langle \Gamma\alpha Jln |\hat{M}^{\vec{P}} | \Gamma^\prime\alpha^\prime
J^\prime l^\prime n^\prime \rangle 
= 
\sum_{m m^\prime}c_{Jlm}^{\,\Gamma \alpha n} \,*
c^{\,\Gamma^\prime \alpha^\prime n^\prime}_{J^\prime l^\prime
  m^\prime}\,\,M^{\vec{P}}_{Jlm,J^\prime l^\prime m^\prime}
\end{equation}
where $c_{Jlm}^{\,\Gamma \alpha n}$ and $c^{\,\Gamma^\prime \alpha^\prime n^\prime}_{J^\prime l^\prime
  m^\prime}$ are relevant Clebsh-Gordan coefficients.
  \begin{table}[!htb]
\resizebox{\textwidth}{!}{%

\begin{tabular}{l | c | c | c | c |c}
$P_{ref} [N_{dir}] $ & Group & $N_{elem}$ & $\Lambda(J):\pi ($ $0^-$ $) $& $\Lambda(J):N ($ $\frac{1}{2}^+$ $)$& $\Lambda(J):\Delta ($ $\frac{3}{2}^+$ $)$ \\
\hline \hline\\
$(0,0,0)$  $[1]$ & $O_{h}^D$  & 96 & $A_{1u}($ $0$ $,4,...)$ & $G_{1g}($ $ \frac{1}{2}$ $,\frac{7}{2},...)\oplus G_{1u}($ $ \frac{1}{2}$ $,\frac{7}{2},...)$ & $H_{g}($ $\frac{3}{2}$ $,\frac{5}{2},...) \oplus H_{u}($ $\frac{3}{2}$ $,\frac{5}{2},...)$ \\  \\
$(0,0,1)$  $[6]$ & $C_{4v}^D$ & 16 & $A_{2}($ $0$ $,1,...)$ & $G_{1}($ $ \frac{1}{2}$ $,\frac{3}{2},...)$ & $G_1(\frac{1}{2},$ $\frac{3}{2}$ $,...) \oplus G_2($ $\frac{3}{2}$ $,\frac{5}{2},...) $\\ \\
$(0,1,1)$  $[12]$ &$C_{2v}^D$ & 8 & $A_{2}($ $0$ $,1,...)$ &  $G($ $\frac{1}{2}$ $,\frac{3}{2},...)$ & $G(\frac{1}{2},$ $\frac{3}{2}$ $,...)$\\ \\
$(1,1,1)$ $[8]$ & $C_{3v}^D$ & 12 & $A_{2}($ $0$ $,1,...)$ &  $G($ $ \frac{1}{2}$ $,\frac{3}{2},...)$ & $G(\frac{1}{2},$ $\frac{3}{2}$ $,...) \oplus F_1($ $\frac{3}{2}$ $,\frac{5}{2},...) \oplus F_2($ $\frac{3}{2}$ $,\frac{5}{2},...)$

\end{tabular}
}
\caption{Frames, Groups $\&$ Irreps $\Lambda$ (with angular momentum content)}
\label{plan}
\end{table}
The advantage of the basis transformation can be observed if we take for e.g. the moving frame $(0,0,1)$, with symmetry group $C^D_{4v}$, having two irreps $G_1$ and $G_2$, then Eq.~(\ref{eq:QC}) for this moving frame simplifies as,
\begin{equation}
\label{eq:g1}
{\rm det}( M^{G1}_{Jln, J'l'n'} - \delta_{JJ'}\delta_{ll'}\delta_{nn'}\cot{\delta^{G1}_{Jl}}) = 0
\end{equation}
\begin{equation}
\label{eq:g2}
{\rm det}( M^{G2}_{Jln, J'l'n'} - \delta_{JJ'}\delta_{ll'}\delta_{nn'}\cot{\delta^{G2}_{Jl}}) = 0
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Resonances
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Resonances} 
The narrow resonances in QCD in the $l$th partial wave of the scattered particles, are characterized by the Breit Wigner(BW) contribution to the $t$-matrix.
\begin{equation}
\label{eq:breit}
t_{\ell}(s) = \frac{\sqrt{s}\,\Gamma(s)}{m_R^2 - s - i \sqrt{s}\,\Gamma(s)},
\end{equation}
where $s$ is the square of center of mass energy (Mandelstam $s$), $m_R$ is the mass of resonance and $\Gamma{(s)}$ is the decay width of the resonance. The decay width $\Gamma(s)$ can be expressed in effective field theory \cite{Pascalutsa:2005vq} to the lowest order expansion as,
\begin{equation}
\label{eq:decay}
\Gamma^{LO}_{EFT}= \frac{ g_{\Delta-\pi N}^2}{48 \pi}\frac{E_N + m_N}{E_N + E_\pi}\frac{p^{*3}}{m_N^2}
\end{equation}
where $g_{\Delta-\pi N}$ is the coupling constant for $\Delta$ resonance, $p^*$ is the center of mass momentum.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Inverse L\"uscher Methodology
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Inverse L\"uscher Methodology}
The L\"uscher quantization condition in Eq.~(\ref{eq:QC}) can be used inversely to obtain lattice spectrum from a given set of resonance parameters $(m_\Delta,\,g_{\Delta-\pi N})$. The mass of the QCD-stable particles on the lattice are calculated as $
m_\pi = 258.3(1.1)\, \textrm{MeV}, \,m_N = 1066.4(2.7)\, \textrm{MeV}$. The QCD-stable masses along with an assumed set of resonance parameters $(m_\Delta,\,g_{\Delta-\pi N})$ in Eq.~(\ref{eq:decay}) and Eq.~(\ref{eq:breit}) we estimate the phase shift  $\delta_{3/2,1}$. Given a moving frame with momentum $\vec{P}$, which fixes the symmetry group, in which we have an irrep $\Lambda$, we can obtain the $n^{th}$ energy level $\sqrt{s_n^{\Lambda,\vec{P}}}^{[model]}$ on the lattice from the $\delta_{3/2,1}$ estimated from our model set of resonance parameters using Eq.~(\ref{eq:QC}).
\begin{figure}[!htb]
\centering
\includegraphics[width=10cm]{Oh_invluscherfinal_spectrum}
\caption{Left Panel: The GEVP spectrum for Irrep $H_g$ in center of mass frame, Center Panel: Stability analysis over different choices of $t_{min}$ for single-exponential fits, the orange band is the final stable spectrum from GEVP. Right Panel: Inverse L\"uscher spectrum from an assumed set of resonance parameters. }
\label{fig:GEVP}
\end{figure} This enables us to define a $\chi^2$ which can be minimized with respect to the resonance parameters to fit the energy levels from our lattice calculations.
\begin{equation}
\chi^2 = \sum_{\vec{P},\Lambda,n} \sum_{\vec{P}^\prime,\Lambda^\prime,n^\prime} \left( \sqrt{s_n^{\Lambda, \vec{P}}}^{[avg]}\
	 - \sqrt{s_n^{\Lambda, \vec{P}}}^{[model]}\right) [C^{-1}]_{\vec{P},\Lambda,n;\vec{P}',\Lambda',n'}
	\left( \sqrt{s_{n'}^{\Lambda', \vec{P}'}}^{[avg]} 
	- \sqrt{s_{n'}^{\Lambda', \vec{P}'}}^{[model]}  \right),
\end{equation}
\begin{figure}[!htb]
\centering
\includegraphics[width=7.5cm]{phase_shift_Oh_C2v}
\caption{Preliminary phase shift analysis results}
\label{phase}
\end{figure} 
The $\chi^2$ is obtained by summing over all the energy levels in the irreps under consideration. The result from the irreps of the two groups $O_h^D$ and $C_{2v}^D$ are presented in Fig.~\ref{phase}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Contemporary results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Comparison with other recent results}
%
Along with our preliminary analysis, we put our results in perspective with other recent results in Table \ref{contemp}. We differ from the other calculations, in terms of analysis methods, in the following ways:
\begin{itemize}
\item Tuned Smearing parameters,
$W\big[ U_{2-HEX} \big]\, D^{-1}\,W\big[ U_{2-HEX}\big]^\dagger$ $(N, \alpha_{WUP}) = (45, 3.0)$
\item We use direct $t$-matrix fits, for estimating the resonance parameters.
\end{itemize}
%
\begin{table}[htb!]
\resizebox{\textwidth}{!}{%
\begin{tabular}{@{}lllll@{}}
\toprule
Collaboration & $m_\pi$(MeV) & Methodology & $m_\Delta$(MeV) & $g_{\Delta-\pi N}$ \\ \midrule
Verduci (2014) \cite{Verduci:2014btc} & 266(3) & Distillation, L\"uscher & 1396(19) & 19.9(83) \\
Alexandrou et.al. (2013) \cite{Alexandrou:2013ata} & 360 & Michael, McNeile & - & 26.7(0.6)(1.4) \\
Alexandrou et.al. (2015) \cite{Alexandrou:2015hxa} & 180 & Michael, McNeile & - & 23.7(0.7)(1.1) \\
Andersen et.al. (2017) \cite{Andersen:2017una} & 280 & Stoch. Distillation, L\"uscher & 1344(20) & 37.1(9.2) \\
Our result(Preliminary) & 258.3(1.1) & Src-smear, L\"uscher & 1414(128) & 26(4) \\
\hline
Physical Value & 139.57018(35) & phen. , K-matrix & 1232(1) & 29.4(3), 28.6(3)
\end{tabular}%
}
\label{contemp}
\caption{Values for $m_R$ and $g_{\Delta-\pi N}$ as obtained in our preliminary analysis compare to other contemporary results with various approaches.}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Outlook
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Outlook}
This preliminary calculation was done with $1/3^{rd}$  of the total measurements available. The phase shift curve will be constrained more precisely with additional points calculated from a bigger lattice $(32^3 \times 48)$ with the same pion mass. We plan to include non-resonant contributions from other possible $J$'s and $l$'s, taking into account partial-wave mixing. We will investigate other decay-width models to better understand model dependence of our resonance parameters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Acknowledgements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Acknowledgements}
We are grateful to BMW Collaboration for providing the gauge ensemble generated with resources
provided by FZ J\"ulich, clusters at Wuppertal and CPT, which are supported by GENCI-[IDRIS/CCRT] grant 52275 . Our calculations were performed at NERSC, supported by the U.S. DOE under Contract
No. DE-AC02-05CH11231. SM and GR are supported by NSF grant PHY-1520996. SM and SS
also thank the RIKEN BNL Research Center for support. JN was supported in part by the DOE Office of Nuclear Physics under grant ~DE{}-SC-0{}011090. AP was supported in part by the U.S. Department of Energy Office of Nuclear Physics under grant DE-{}FC02-06ER41444. SP is supported by the Horizon 2020 of the European Commission research and innovation programme under the Marie Sklodowska-Curie grant agreement No. 642069. We acknowledge the use of USQCD software QLUA extensively for all the calculations of correlators.
\bibliography{lattice2018}

\end{document}
